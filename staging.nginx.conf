upstream api {  
    server events-api:5000;
}

server {
    location /.well-known/acme-challenge/ {
        allow all;
        root /var/www/html;
        try_files $uri =404;
        break;
    }

    location / {
        return 301 https://$server_name$request_uri;
    }

    listen 80;
    listen [::]:80;
 
    server_name staging.events.excelmec.org;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name staging.accounts.excelmec.org;

    server_tokens off;

    ssl_certificate /etc/letsencrypt/live/staging.events.excelmec.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/staging.events.excelmec.org/privkey.pem;

    ssl_buffer_size 8k;

    ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;

    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    ssl_session_tickets off;

    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8;

    location /api/ {
        proxy_pass http://api/;
        proxy_set_header Host $host;
    }

}

