namespace API.Dtos.Registration
{
    public class DataForRegistrationDto
    {
        public int EventId { get; set; }
        public int? TeamId { get; set; }
    }
}