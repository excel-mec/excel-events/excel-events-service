namespace API.Dtos.Registration
{
    public class DataForRegistrationByAdminDto
    {
        public int ExcelId { get; set; }
        public DataForRegistrationDto RegistrationData { get; set; }
    }
}