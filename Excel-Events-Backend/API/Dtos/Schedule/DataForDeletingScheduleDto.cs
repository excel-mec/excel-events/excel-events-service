namespace API.Dtos.Schedule
{
    public class DataForDeletingScheduleDto
    {
        public int EventId { get; set; }
        public int RoundId { get; set; }
    }
}