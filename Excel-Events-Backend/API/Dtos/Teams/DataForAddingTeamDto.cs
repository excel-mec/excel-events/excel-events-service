﻿namespace API.Dtos.Teams
{
    public class DataForAddingTeamDto
    {
        public string Name { get; set; }
        public int EventId { get; set; }
    }
}