﻿// <auto-generated />
using System;
using API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace API.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20201227183418_AddResultsTable")]
    partial class AddResultsTable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityByDefaultColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity("API.Models.Bookmark", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<int>("EventId")
                        .HasColumnType("integer");

                    b.Property<int>("ExcelId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.ToTable("Bookmarks");
                });

            modelBuilder.Entity("API.Models.Event", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("About")
                        .HasColumnType("text");

                    b.Property<string>("Button")
                        .HasColumnType("text");

                    b.Property<int>("CategoryId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasDefaultValue(0);

                    b.Property<int?>("CurrentRound")
                        .HasColumnType("integer");

                    b.Property<DateTime>("Datetime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int?>("Day")
                        .HasColumnType("integer");

                    b.Property<int?>("EntryFee")
                        .HasColumnType("integer");

                    b.Property<int?>("EventHead1Id")
                        .HasColumnType("integer");

                    b.Property<int?>("EventHead2Id")
                        .HasColumnType("integer");

                    b.Property<int>("EventStatusId")
                        .HasColumnType("integer");

                    b.Property<int>("EventTypeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasDefaultValue(0);

                    b.Property<string>("Format")
                        .HasColumnType("text");

                    b.Property<string>("Icon")
                        .HasColumnType("text");

                    b.Property<bool>("IsTeam")
                        .HasColumnType("boolean");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<bool?>("NeedRegistration")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("boolean")
                        .HasDefaultValue(true);

                    b.Property<int?>("NumberOfRounds")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasDefaultValue(0);

                    b.Property<int?>("PrizeMoney")
                        .HasColumnType("integer");

                    b.Property<DateTime?>("RegistrationEndDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("RegistrationLink")
                        .HasColumnType("text");

                    b.Property<bool?>("RegistrationOpen")
                        .HasColumnType("boolean");

                    b.Property<string>("Rules")
                        .HasColumnType("text");

                    b.Property<int?>("TeamSize")
                        .HasColumnType("integer");

                    b.Property<string>("Venue")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("EventHead1Id");

                    b.HasIndex("EventHead2Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Events");
                });

            modelBuilder.Entity("API.Models.EventHead", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Email")
                        .HasColumnType("text");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.ToTable("EventHeads");
                });

            modelBuilder.Entity("API.Models.Highlight", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Image")
                        .HasColumnType("text");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Highlights");
                });

            modelBuilder.Entity("API.Models.Registration", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<int>("EventId")
                        .HasColumnType("integer");

                    b.Property<int>("ExcelId")
                        .HasColumnType("integer");

                    b.Property<int?>("TeamId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("TeamId");

                    b.HasIndex("EventId", "ExcelId")
                        .IsUnique();

                    b.ToTable("Registrations");
                });

            modelBuilder.Entity("API.Models.Result", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<int>("EventId")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<int>("Position")
                        .HasColumnType("integer");

                    b.Property<string>("TeamMembers")
                        .HasColumnType("text");

                    b.Property<string>("TeamName")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.ToTable("Results");
                });

            modelBuilder.Entity("API.Models.Schedule", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<DateTime>("Datetime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("Day")
                        .HasColumnType("integer");

                    b.Property<int>("EventId")
                        .HasColumnType("integer");

                    b.Property<string>("Round")
                        .HasColumnType("text");

                    b.Property<int>("RoundId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.ToTable("Rounds");
                });

            modelBuilder.Entity("API.Models.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn()
                        .HasIdentityOptions(2246L, 37L, null, null, null, null);

                    b.Property<int>("EventId")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.HasIndex("Name", "EventId")
                        .IsUnique();

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("API.Models.Bookmark", b =>
                {
                    b.HasOne("API.Models.Event", "Event")
                        .WithMany("Bookmarks")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Event");
                });

            modelBuilder.Entity("API.Models.Event", b =>
                {
                    b.HasOne("API.Models.EventHead", "EventHead1")
                        .WithMany()
                        .HasForeignKey("EventHead1Id")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("API.Models.EventHead", "EventHead2")
                        .WithMany()
                        .HasForeignKey("EventHead2Id")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.Navigation("EventHead1");

                    b.Navigation("EventHead2");
                });

            modelBuilder.Entity("API.Models.Registration", b =>
                {
                    b.HasOne("API.Models.Event", "Event")
                        .WithMany("Registrations")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("API.Models.Team", "Team")
                        .WithMany("Registrations")
                        .HasForeignKey("TeamId");

                    b.Navigation("Event");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("API.Models.Result", b =>
                {
                    b.HasOne("API.Models.Event", "Event")
                        .WithMany("Results")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Event");
                });

            modelBuilder.Entity("API.Models.Schedule", b =>
                {
                    b.HasOne("API.Models.Event", "Event")
                        .WithMany("Rounds")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Event");
                });

            modelBuilder.Entity("API.Models.Team", b =>
                {
                    b.HasOne("API.Models.Event", "Event")
                        .WithMany("Teams")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Event");
                });

            modelBuilder.Entity("API.Models.Event", b =>
                {
                    b.Navigation("Bookmarks");

                    b.Navigation("Registrations");

                    b.Navigation("Results");

                    b.Navigation("Rounds");

                    b.Navigation("Teams");
                });

            modelBuilder.Entity("API.Models.Team", b =>
                {
                    b.Navigation("Registrations");
                });
#pragma warning restore 612, 618
        }
    }
}
